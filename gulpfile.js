'use strict';
var gulp = require('gulp'),
  notify = require('gulp-notify'),
  autoprefixer = require('gulp-autoprefixer'),
  concat = require('gulp-concat'),
  combinemq = require('gulp-group-css-media-queries'),
  imagemin = require('gulp-imagemin'),
  jshint = require('gulp-jshint'),
  stylish = require('jshint-stylish'),
  minifycss = require('gulp-minify-css'),
  rename = require('gulp-rename'),
  sass = require('gulp-ruby-sass'),
  shell = require('gulp-shell'),
  size = require('gulp-size'),
  uglify = require('gulp-uglify'),
  pngcrush = require('imagemin-pngcrush'),
  stripmq = require('gulp-stripmq');

var config = {
  sass: './scss/**/*.scss',
  css: './css',
  css_nomq: './css/nomq',
  js: './js',
  images: './images/*.{png,gif,jpeg,jpg,svg}',
  imagesmin: './images/minified',
  bower: './bower_components'
};

var autoprefixer_browsers = [
  '> 3%'
];

gulp.task('styles-nomq', function() {
  return sass(config.sass, {
    style: 'expanded',
    precision: 6
  })
    .pipe(stripmq({
      options: {width: 1024},
      type: 'screen'
    }))
    .pipe(autoprefixer({
      browsers: autoprefixer_browsers,
      cascade: false
    }))
    .pipe(gulp.dest(config.css_nomq))
    .pipe(minifycss())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(config.css_nomq + '/minified'))
    .pipe(size({title: 'css-nomq'}));
});

gulp.task('styles', function() {
  return sass(config.sass, {
    style: 'expanded',
    precision: 6
  })
    .pipe(combinemq())
    .pipe(autoprefixer({
      browsers: autoprefixer_browsers,
      cascade: false
    }))
    .pipe(gulp.dest(config.css))
    .pipe(minifycss())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(config.css + '/minified'))
    .pipe(size({title: 'css'}));
});

gulp.task('concat_scripts', function() {
  return gulp.src(config.js + '/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter(stylish))
    .pipe(concat('scripts_concat.js'))
    .pipe(gulp.dest(config.js + '/build'));
});

gulp.task('productionjs', ['concat_scripts'], function() {
  return gulp.src([
    config.bower + '/fastclick/lib/fastclick.js',
    config.bower + '/owl.carousel/dist/owl.carousel.js',
    config.bower + '/tableit/tableit.js',
    config.bower + '/mediaWrapper.js/jquery.mediaWrapper.js',
    config.js + '/build/scripts_concat.js'
  ])
    .pipe(concat('production.js'))
    .pipe(uglify())
    .pipe(gulp.dest(config.js + '/build'))
    .pipe(size({title: 'production js'}));
});

gulp.task('images', function() {
  return gulp.src(config.images)
    .pipe(imagemin({
      optimizationLevel: 7,
      progressive: true,
      interlaced: true
    }))
    .pipe(gulp.dest(config.imagesmin))
    .pipe(size({title: 'images'}));
});

gulp.task('drush', shell.task([
  'drush cc theme-registry'
]));

gulp.task('default', ['styles-nomq', 'styles', 'productionjs', 'images']);

gulp.task('watch', [], function() {
  gulp.watch(config.sass, ['styles-nomq']);
  gulp.watch(config.sass, ['styles']);
  gulp.watch(config.js + '/*.js', ['productionjs']);
  gulp.watch(config.images, ['images']);
});

gulp.task('watch-drush', [], function() {
  gulp.watch(config.sass, ['styles-nomq']);
  gulp.watch(config.sass, ['styles']);
  gulp.watch(config.js + '/*.js', ['productionjs']);
  gulp.watch(config.images, ['images']);
  gulp.watch('**/*.{php,inc,info}', ['drush']);
});