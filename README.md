# <custom_theme> Responsive Theme #

## Build Requirements ##
* Sass
* NodeJS/npm
* Bower

### Initial Setup ###
* Navigate to the root of the theme: `sites/all/themes/<custom_theme>`
* Run `npm install`
* Run `gulp`

### Theme structure ###
* Theme root
    * css
    * fonts
    * images
    * js
    * scss
    * templates

### Ongoing Development ###
The build process is managed by gulp. The following tasks are defined:

* `styles-nomq`: compiles the scss file and then strips the media queries that apply to widths
less than 1024px. The resulting file is then autoprefixed and minified.
* `styles`: compiles the scss file, combines all media queries, then autoprefixes and minifies 
the file.
* `concat_scripts`: concatenates all custom scripts residing in the `js` folder.
* `productionjs`: concatenates all bower-managed libraries with the concatenated file from the 
`concat_scripts` task into one minified file named `production.js`.
* `images`: optimizes the images residing in the `images` folder.
* `drush`: clears the drupal theme registry cache.
* `default`: runs the `styles_nomq`, `styles`, `productionjs`, and `images` tasks.
* `watch`: watches for changes to files in the scss, js, or images directories, and runs the
corresponding tasks when changes are detected.
* `watch-drush`: same as `watch`, but also watches for changes to php, .inc, or .info files and
and clears the drupal theme registry cache when changes are detected. This watch depends on the
drupal database being accessible to drush.

